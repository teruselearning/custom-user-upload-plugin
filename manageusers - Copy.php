<?php

require('../../../config.php');
require_once($CFG->libdir.'/adminlib.php');
require_once($CFG->libdir.'/formslib.php');
require_once('locallib.php');
require_once($CFG->dirroot.'/group/lib.php');
require_once($CFG->dirroot.'/user/profile/lib.php');
require_once($CFG->dirroot.'/user/lib.php');

class csv_data_form extends moodleform {
	
	public function definition() {
		global $USER, $OUTPUT,$DB;
         
		$mform =& $this->_form;
		$courseid = $this->_customdata['courseid'];
		$roleid = $this->_customdata['roleid'];
		$groupid = $this->_customdata['groupid'];
		$func = $this->_customdata['func'];

		$mform->addElement('hidden', 'courseid', $courseid);
		$mform->setType('courseid', PARAM_RAW);
		$mform->addElement('hidden', 'roleid', $roleid);
		$mform->setType('roleid', PARAM_RAW);
		$mform->addElement('hidden', 'func', $func);
		$mform->setType('func', PARAM_RAW);
		
		$users = $DB->get_records('tool_custom_uploaduser');
		// $mform->addElement('html', '<div>'.$func.'</div>');
		$mform->addElement('html', '<div class="userheader"><table cellpadding="10" class="userheadertable" ><th>First Name</th><th>Surname</th><th>Email address</th><th>Password</th><th></th></table></div>');
		$i=0;
		foreach($users as $user){

			//Does the user exist?
			if($func == "existing"){
				$existingstatus = validate_email_existing($user->email);
			}else{
				$existingstatus = "";
			}
			


			$availablefromgroup=array();
			$availablefromgroup[] =& $mform->createElement('text', 'firstname'.$i, 'firstname:','class="userdetails"');
			$availablefromgroup[] =& $mform->createElement('text', 'lastname'.$i, 'lastname:','class="userdetails" ');
			$availablefromgroup[] =& $mform->createElement('text', 'email'.$i, 'email:','class="userdetails emailinput" ');
			$availablefromgroup[] =& $mform->createElement('text', 'password'.$i, 'password:','class="userdetails" ');
			$availablefromgroup[] =& $mform->createElement('static', 'statustext'.$i, 'status:','class="userdetails status" ');
			$availablefromgroup[] = &$mform->createElement('button','delete'.$i,
											 html_writer::empty_tag('img', array('src'=>$OUTPUT->image_url('i/invalid'), 'alt'=>'Remove user', 'class'=>'iconsmall')),
											 'class="open-homeEvents" data-id="'.$user->id.'" data-toggle="modal" data-target="#modalForm"');
			$availablefromgroup[] =& $mform->createElement('hidden', 'status'.$i, '');
			$availablefromgroup[] =& $mform->createElement('hidden', 'detailid'.$i, '');
			$availablefromgroup[] =& $mform->createElement('hidden', 'valid'.$i, '');
			$mform->addGroup($availablefromgroup, 'availablefromgroup', '', ' ', false);
			$mform->setDefault('firstname'.$i, $user->firstname);
			$mform->setDefault('lastname'.$i, $user->lastname);
			$mform->setDefault('email'.$i, $user->email);
			$mform->setDefault('password'.$i, $user->password);
			$mform->setDefault('status'.$i, $user->status.$existingstatus);
			$mform->setDefault('statustext'.$i, $user->status);
			$mform->setDefault('detailid'.$i, $user->id);
			$mform->setDefault('valid'.$i, $user->valid);

			$mform->setType('firstname'.$i, PARAM_RAW);
			$mform->setType('lastname'.$i, PARAM_RAW);
			$mform->setType('email'.$i, PARAM_RAW);
			$mform->setType('password'.$i, PARAM_RAW);
			$mform->setType('status'.$i, PARAM_RAW);
			$mform->setType('statustext'.$i, PARAM_RAW);
			$mform->setType('detailid'.$i, PARAM_RAW);
			$mform->setType('valid'.$i, PARAM_RAW);
			$i++;
		}
		$mform->addElement('hidden', 'repeatid', '');
		$mform->setDefault('repeatid', $i);
		$mform->setType('repeatid', PARAM_RAW);
		
		$availablefromgroup=array();
		$availablefromgroup[] =& $mform->createElement('text', 'firstname', 'firstname:','class="userdetails" ');
		$availablefromgroup[] =& $mform->createElement('text', 'lastname', 'lastname:','class="userdetails" ');
		$availablefromgroup[] =& $mform->createElement('text', 'email', 'email:','class="userdetails" ');
		$availablefromgroup[] =& $mform->createElement('text', 'password', 'password:','class="userdetails"');
				
		$repeateloptions = array();

		$repeateloptions['firstname']['type'] = PARAM_RAW;
		$repeateloptions['lastname']['type'] = PARAM_RAW;
		$repeateloptions['email']['type'] = PARAM_RAW;
		$repeateloptions['password']['type'] = PARAM_RAW;
		$repeateloptions['status']['type'] = PARAM_RAW;

		$repeatarray = array();
		$repeatarray[$i] = $mform->createElement('group', 'groupname','', $availablefromgroup, null, false);

		$this->repeat_elements($repeatarray, 0,
				 $repeateloptions, 'option_repeats', 'option_add_fields', 3, null, true);
				 
		$this->add_action_buttons();

    }
}
$courseid = optional_param('courseid', 0, PARAM_INT);
$roleid = optional_param('roleid', 0, PARAM_INT);
$groupid = optional_param('groupid', 0, PARAM_INT);
$action = optional_param('action', '', PARAM_RAW);  
$sesskey = optional_param('sesskey', '', PARAM_RAW);  
$func = optional_param('func', '', PARAM_RAW);  
$confirm = optional_param('confirm', 0, PARAM_BOOL);
$id = optional_param('id', '', PARAM_INT);

require_login();
admin_externalpage_setup('toolcustom_uploaduser');
require_capability('tool/custom_uploaduser:uploadusers', context_system::instance());
$PAGE->requires->jquery();
$PAGE->requires->js('/admin/tool/custom_uploaduser/js/custom.js', true);
//$returnurl = new moodle_url('/admin/tool/custom_uploaduser/index.php');
$returnurl ='/admin/tool/custom_uploaduser/index.php';
$url ='/admin/tool/custom_uploaduser/manageusers.php';

$params = ['courseid' => $courseid ,
		  'roleid' => $roleid ,
		  'groupid' => $groupid ,
		  'func' => $func ,
		];
		
if ($action == 'delete' ) {
    $user = $DB->get_record('tool_custom_uploaduser', array('id' => $id));
    if ($user) {
        $DB->delete_records('tool_custom_uploaduser', array('id' => $id));
    }
    redirect(new moodle_url($url, $params),"User Deleted",null,\core\output\notification::NOTIFY_SUCCESS); 

}                
$mform = new csv_data_form(null,$params,$method='post', $target='', array('id'=>"manageusers"));

// If data submitted, then process and store.
if ($mform->is_cancelled()) {
	redirect(new moodle_url($returnurl, $params),"User upload cancelled",null,\core\output\notification::NOTIFY_ERROR); 
   
} else if ($data = $mform->get_data()) {
	$repeatid = $data->repeatid;
	for($i=0;$i<$repeatid ;$i++){
		if(empty($_POST['email'.$i])){
			//delete row from temp table if no email
			$DB->delete_records('tool_custom_uploaduser', ['id' => $_POST['detailid'.$i]]);
		}
		else{
			if(isset($_POST['email'.$i]) && !validate_email($_POST['email'.$i])){
				$_POST['status'.$i] = '<span class="statusred">Invalid</span>';
				$_POST['valid'.$i] = 0;
			}
			else if(empty($_POST['firstname'.$i]) || empty($_POST['lastname'.$i])){
				$_POST['status'.$i] = '<span class="statusred">Name required</span>';
				$_POST['valid'.$i] = 0;
			}
			else if(isset($_POST['email'.$i]) && validate_csv_email($_POST['email'.$i])){
				$_POST['status'.$i] = validate_csv_email($_POST['email'.$i]);
				$_POST['valid'.$i] = 1;//To enrol existing users
			}
			else{
				$_POST['status'.$i] = '<span class="statusgreen">Validated</span>';
				$_POST['valid'.$i] = 1;
			}
			$record = new stdClass();
			$record->id = $_POST['detailid'.$i];
			$record->firstname = $_POST['firstname'.$i];
			$record->lastname = $_POST['lastname'.$i];
			$record->email = $_POST['email'.$i];
			$record->password = $_POST['password'.$i];
			$record->status = $_POST['status'.$i];
			$record->valid = $_POST['valid'.$i];
			$DB->update_record('tool_custom_uploaduser', $record);
		}

	}
	if(isset($_POST['email'])){
		$firstnames=$_POST['firstname'];
		$lastnames=$_POST['lastname'];
		$emails=$_POST['email'];
		$passwords=$_POST['password'];
		
		if(count($emails) > 0){
			foreach($emails as $key=>$value){
				if(!empty($value)){
					if(validate_csv_email($value)){
						$status[$key] = validate_csv_email($value);
						$valid[$key] = 0;
					}
					else if(empty($firstnames[$key]) || empty($lastnames[$key])){
						$status[$key] =  '<span class="statusred">Name required</span>';
						$valid[$key] = 0;
					}
					else{
						$status[$key] ='<span class="statusgreen">Validated</span>';
						$valid[$key] = 1;
					}
					$record = new stdClass();
					$record->firstname = $firstnames[$key];
					$record->lastname = $lastnames[$key];
					$record->email = $emails[$key];
					$record->password = $passwords[$key];
					$record->status = $status[$key];
					$record->valid = $valid[$key];
					$DB->insert_record('tool_custom_uploaduser', $record);								
				}
			}
		}	
	}
	$tempusers = $DB->get_records('tool_custom_uploaduser',array('valid'=>0));
	if($tempusers)
		redirect(new moodle_url($url, $params),"please correct invalid data ",null,\core\output\notification::NOTIFY_ERROR); 
	else{
		//upload users
		 if (enrol_is_enabled('manual')) {
			$manual = enrol_get_plugin('manual');
		} else {
			$manual = NULL;
		}
		$tempusers = $DB->get_records('tool_custom_uploaduser',array('valid'=>1));
		foreach($tempusers as $user){
			$user->username = core_user::clean_field($user->email, 'username');
            $user->mnethostid = $CFG->mnet_localhost_id;
			if ($existinguser = $DB->get_record('user', array('username'=>$user->username, 'mnethostid'=>$user->mnethostid))) {
				$user->id = $existinguser->id;
				$user = $existinguser;
			}
			else{
				$user->confirmed    = 1;
				$user->timemodified = time();
				$user->timecreated  = time();
				$user->suspended = 0;
				$user->auth = 'manual';
				$user->lang = '';
				$forcechangepassword = false;

				if($auth = get_auth_plugin($user->auth)){
					$isinternalauth = $auth->is_internal();
					if ($isinternalauth) {
						if (empty($user->password)) {
							$user->password = 'to be generated';							
						}
						else{
							$errmsg = null;
							$weak = !check_password_policy($user->password, $errmsg);	
							if ($weak) {
								$forcechangepassword = true;
							}
						}
					}
				}
				$user->id = user_create_user($user, false, false);
				
				profile_save_data($user);
				
				if ($forcechangepassword) {
					set_user_preference('auth_forcepasswordchange', 1, $user);
				}
				if ($user->password === 'to be generated') {
					set_user_preference('create_password', 1, $user);
				}

				// Trigger event.
				\core\event\user_created::create_from_userid($user->id)->trigger();


			}
            // make sure user context exists
            context_user::instance($user->id);
            
			// find course enrolments, groups, roles and enrol periods

			$courseid      = $data->courseid;
            $coursecontext = context_course::instance($courseid);
			if ($manual) {
				if ($instances = enrol_get_instances($courseid, false)) {
					foreach ($instances as $instance) {
						if ($instance->enrol === 'manual') {
							$courseinstance = $instance;
							break;
						}
					}
				}
			}	
			if ($manual and $courseinstance) {
				$roleid = $data->roleid;
				if ($roleid) {
                    // Find duration and/or enrol status.
                    $timeend = 0;
                    $status = null;
                    $today = time();
                    if ($courseinstance->enrolperiod > 0) {
                        $timeend = $today + $courseinstance->enrolperiod;
                    }
                    $manual->enrol_user($courseinstance, $user->id, $roleid, $today, $timeend, $status);

				}
			}	
			
			// make sure user is enrolled into course before adding into groups
			if (is_enrolled($coursecontext, $user->id)) {
					groups_add_member($data->groupid, $user->id);
			}	
							
		}
		
		$DB->delete_records('tool_custom_uploaduser');

		redirect(new moodle_url($returnurl),"Users uploaded ",null,\core\output\notification::NOTIFY_SUCCESS); 
		
	}

	
}
// Print the header
echo $OUTPUT->header();

echo $OUTPUT->heading(get_string('uploaduserspreview', 'tool_custom_uploaduser'));

echo $mform->display();
    echo '
     <div class="modal fade" id="modalForm" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">      <!-- Modal Header -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Confirmation</h4>
                </div>

                <!-- Modal Body -->
                <div class="modal-body">
                    <p class="statusMsg">Are you sure you want to delete this user?</p>
                    <form role="form" action="manageusers.php" id="deleteform">
                    <input type="hidden" name="id" id="rowId" value=""/>
                    <input type="hidden" name="courseid" id="rowId" value="'.$courseid.'"/>
                    <input type="hidden" name="roleid" id="rowId" value="'.$roleid.'"/>
                    <input type="hidden" name="groupid" id="rowId" value="'.$groupid.'"/>
                    <input type="hidden" name="action" value="delete"/>
                     </form>
                </div>

                <!-- Modal Footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" id="submitForm" class="btn btn-primary submitBtn" >SUBMIT</button>
                </div>
            </div>
        </div>
    </div>';

echo $OUTPUT->footer();
die;
