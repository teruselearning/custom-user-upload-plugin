<?php

require('../../../config.php');

$courseid = optional_param('courseid', '', PARAM_RAW);
$groupid = optional_param('groupid', '', PARAM_RAW);
$returnarray = array();

if($course = $DB->get_record('course', array('id' => $courseid))){
	$returnarray["coursename"] = $course->fullname;
}else{
	$returnarray["coursename"] = "";
}

if($group = $DB->get_record('groups', array('id' => $groupid))){
	$returnarray["groupname"] = $group->name;
}else{
	$returnarray["groupname"] = "";
}

echo json_encode($returnarray);

?>