<?php

require('../../../config.php');

$courseid = optional_param('value', '', PARAM_RAW);
global $DB;
	
	$groupobj = $DB->get_records('groups',array('courseid'=>$courseid));
	$groups= array('0'=>"Choose");
	$groups[1] = "Add a new group"; 
    if ($groupobj) {

        foreach($groupobj as $group){
		$groups[$group->id] = $group->name; 
		}
    }
   

	echo json_encode($groups);
	
	
?>
