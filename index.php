<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Bulk user registration script from a comma separated file
 *
 * @package    tool
 * @subpackage custom_uploaduser
 * @copyright  2004 onwards Martin Dougiamas (http://dougiamas.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require('../../../config.php');
require_once($CFG->libdir.'/adminlib.php');
require_once($CFG->libdir.'/csvlib.class.php');
require_once($CFG->dirroot.'/user/profile/lib.php');
require_once($CFG->dirroot.'/user/lib.php');
require_once($CFG->dirroot.'/group/lib.php');
require_once($CFG->dirroot.'/cohort/lib.php');
require_once('locallib.php');
require_once('user_form.php');

$iid         = optional_param('iid', '', PARAM_INT);
$previewrows = optional_param('previewrows', 100, PARAM_INT);
$courseid = optional_param('courseid', 0, PARAM_INT);
$roleid = optional_param('roleid', 0, PARAM_INT);
$groupid = optional_param('groupid', 0, PARAM_INT);
$func = optional_param('func', 0, PARAM_INT);

core_php_time_limit::raise(60*60); // 1 hour should be enough
raise_memory_limit(MEMORY_HUGE);

require_login();
admin_externalpage_setup('toolcustom_uploaduser');
require_capability('tool/custom_uploaduser:uploadusers', context_system::instance());
$PAGE->requires->jquery();
$PAGE->requires->js('/admin/tool/custom_uploaduser/js/custom.js', true);

$url = new moodle_url('/admin/tool/custom_uploaduser/index.php');
$noticeurl ='/admin/tool/custom_uploaduser/index.php';
$redirecturl ='/admin/tool/custom_uploaduser/manageusers.php';
$bulknurl  = new moodle_url('/admin/user/user_bulk.php');

$today = time();
$today = make_timestamp(date('Y', $today), date('m', $today), date('d', $today), 0, 0, 0);

// array of all valid fields for validation
$STD_FIELDS = array('id', 'username', 'email',
        'city', 'country', 'lang', 'timezone', 'mailformat',
        'maildisplay', 'maildigest', 'htmleditor', 'autosubscribe',
        'institution', 'department', 'idnumber', 'skype',
        'msn', 'aim', 'yahoo', 'icq', 'phone1', 'phone2', 'address',
        'url', 'description', 'descriptionformat', 'password',
        'auth',        // watch out when changing auth type or using external auth plugins!
        'oldusername', // use when renaming users - this is the original username
        'suspended',   // 1 means suspend user account, 0 means activate user account, nothing means keep as is for existing users
        'deleted',     // 1 means delete user
        'mnethostid',  // Can not be used for adding, updating or deleting of users - only for enrolments, groups, cohorts and suspending.
        'interests',
    );
// Include all name fields.
$STD_FIELDS = array_merge($STD_FIELDS, get_all_user_name_fields());

$PRF_FIELDS = array();

if ($proffields = $DB->get_records('user_info_field')) {
    foreach ($proffields as $key => $proffield) {
        $profilefieldname = 'profile_field_'.$proffield->shortname;
        $PRF_FIELDS[] = $profilefieldname;
        // Re-index $proffields with key as shortname. This will be
        // used while checking if profile data is key and needs to be converted (eg. menu profile field)
        $proffields[$profilefieldname] = $proffield;
        unset($proffields[$key]);
    }
}

if($courseid >0 && $roleid > 0 && $groupid > 0){
	$params = ['courseid' => $courseid ,
		  'roleid' => $roleid ,
		  'groupid' => $groupid
		];
	$mform1 = new admin_custom_uploaduser_form(null,$params);
	
}
else
	$mform1 = new admin_custom_uploaduser_form();

if ($formdata = $mform1->get_data()) {
	
	if($formdata->courseid == 0){
		redirect($url,"Please select a course",null,\core\output\notification::NOTIFY_ERROR);
	}

	if ($formdata->func && $formdata->func == "newuser") {
		$newuserform = true;
		echo "doing new user stuff";
	} else if ($formdata->func && $formdata->func == "existinguser") {
		$newuserform = false;
		echo "doing existing stuff";
	}
		
	if(!empty($formdata->newgroup)){
		$newgroup = trim($formdata->newgroup);
		// if group doesn't exist,  create it
		$newgroupdata = new stdClass();
		$newgroupdata->name = $newgroup;
		$newgroupdata->courseid = $formdata->courseid;
		$newgroupdata->description = '';
		$formdata->groupid = groups_create_group($newgroupdata);
	}
	else if(isset($_POST['groupid'])){
		if($_POST['groupid'] == 1){
			redirect($url,"Please name your group",null,\core\output\notification::NOTIFY_ERROR);
		}
		$formdata->groupid = $_POST['groupid'];
	}
	
	if(empty($formdata->groupid)){
		$formdata->groupid = null;
	}
		
	$urlparams = ['courseid' => $formdata->courseid ,
			  'roleid' => $formdata->roleid,
			  'groupid' => $formdata->groupid,
			  'func' => $formdata->func
			];

	if(isset($formdata->addbutton) && $formdata->addbutton == get_string('oradd','tool_custom_uploaduser'))
		  redirect(new moodle_url($redirecturl, $urlparams));
	else if(empty($mform1->get_file_content('userfile')) && empty($formdata->fromexcel)){
		$users = $DB->get_records('tool_custom_uploaduser');
		if(!$users)
			redirect(new moodle_url($noticeurl, $urlparams),"Please upload data",null,\core\output\notification::NOTIFY_ERROR);
		else{
			redirect(new moodle_url($redirecturl, $urlparams));
		}
	}
	$validateuser = false;	
	$content = $mform1->get_file_content('userfile');
	if($content){
		$DB->delete_records('tool_custom_uploaduser');
		$validateuser = true;	

		$iid = csv_import_reader::get_new_iid('custom_uploaduser');
		$cir = new csv_import_reader($iid, 'custom_uploaduser');

		$readcount = $cir->load_csv_content($content, 'UTF-8', 'comma');
		$csvloaderror = $cir->get_error();
		unset($content);
		
		if (!is_null($csvloaderror)) {
			print_error('csvloaderror', '', $returnurl, $csvloaderror);
		}
		$filecolumns = uu_validate_user_upload_columns($cir, $STD_FIELDS, $PRF_FIELDS, $url);

		$data = array();
		$cir->init();
		$linenum = 1; //column header is first line
		$noerror = true; // Keep status of any error.
		while ($linenum <= $previewrows and $fields = $cir->next()) {
			$linenum++;
			$rowcols = array();
			foreach($fields as $key => $field) {
				$rowcols[$filecolumns[$key]] = s(trim($field));
			}
			$rowcols['status'] = array();
			$rowcols['valid'] = 0;

			if (isset($rowcols['email']) && validate_csv_email($rowcols['email'])) {
				   $rowcols['status'][] = validate_csv_email($rowcols['email']);

			}
			if (isset($rowcols['email']) && validate_email_existing($rowcols['email'])) {
				   $rowcols['status'][] .= validate_email_existing($rowcols['email']);

			}
			if(empty($rowcols['status'])){
				$rowcols['status'][] = '<span class="notif statusgreen">Validated</span>';
				$rowcols['valid']= 1;
			}
			// Check if rowcols have custom profile field with correct data and update error state.
			$noerror = uu_check_custom_profile_data($rowcols) && $noerror;
			$rowcols['status'] = implode('<br />', $rowcols['status']);
			$data[] = $rowcols;
		}
		if ($fields = $cir->next()) {
			$data[] = array_fill(0, count($fields) + 2, '...');
		}
		$cir->close();

		foreach($data as $key=>$row){
			$record = new stdClass();
			foreach($row as $column=>$value){
				$record->$column = $value;
			}
				$record->id = $DB->insert_record('tool_custom_uploaduser', $record);

		}
	}
	else if(!empty($formdata->fromexcel)){
		//fromexcel
		$DB->delete_records('tool_custom_uploaduser');
		$content = $formdata->fromexcel;
		$rowsarray = preg_split("/\\r\\n|\\r|\\n/", $content); 
		$fielddrow  = $rowsarray[0];

		$filecolumns = explode("\t",$fielddrow);
		$headercount = count($filecolumns);
		$data = array();

		unset($rowsarray[0]);
		foreach($rowsarray as $rows){
			if(!empty($rows)){
				$fields = explode("\t",$rows);
				$rowcols = array();
				foreach($fields as $key => $field) {
					$rowcols[$filecolumns[$key]] = s(trim($field));
				}
				$rowcount = count($rowcols);
				if($headercount == $rowcount){
						
					$rowcols['status'] = "";
					$rowcols['valid'] = 0;

					if (isset($rowcols['email']) && !validate_email($rowcols['email'])) {
						   $rowcols['status'] = '<span class="notif statusred">Invalid email</span>';

					}
					if($formdata->func == "existinguser" || $formdata->func == "existing"){
						if (isset($rowcols['email']) && validate_email_existing($rowcols['email'])) {
							$rowcols['status'] .= '<span class="notif statusgreen">User exists</span>';
						}else if(isset($rowcols['email']) && !validate_email_existing($rowcols['email'])){
							$rowcols['status'] .= '<span class="notif statusred">Email not found</span>';
						}
					}else{
						if (isset($rowcols['email']) && validate_email_existing($rowcols['email'])) {
							   $rowcols['status'] .= '<span class="notif statusred">User exists</span>';
						}					
					}
				
					if(empty($rowcols['status'])){
						$rowcols['status'] = '<span class="notif statusgreen">Valid</span>';
						$rowcols['valid']= 1;
					}
					$data[] = $rowcols;	
				}
			}	
		}
		foreach($data as $key=>$row){
			$record = new stdClass();
			foreach($row as $column=>$value){
				$record->$column = $value;
			}
			$validateuser = true;				
			$record->id = $DB->insert_record('tool_custom_uploaduser', $record);

		}		
	}
	if($validateuser)
		redirect(new moodle_url($redirecturl, $urlparams));
	else
		redirect($url,"Please check data",null,\core\output\notification::NOTIFY_ERROR);

} 
	
	echo $OUTPUT->header();

	echo $OUTPUT->heading_with_help(get_string('custom_uploadusers', 'tool_custom_uploaduser'), 'custom_uploadusers', 'tool_custom_uploaduser');

	$mform1->display();
	echo $OUTPUT->footer();







