<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Bulk user upload forms
 *
 * @package    tool
 * @subpackage custom_uploaduser
 * @copyright  2007 Dan Poltawski
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once $CFG->libdir.'/formslib.php';
require_once($CFG->dirroot . '/user/editlib.php');

/**
 * Upload a file CVS file with user information.
 *
 * @copyright  2007 Petr Skoda  {@link http://skodak.org}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class admin_custom_uploaduser_form extends moodleform {
    function definition () {
		global $DB, $USER;
        $mform = $this->_form;

        $mform->addElement('header', 'generalheader', get_string('general'));
        $mform->addElement('static', 'generalinfo', get_string('generalinfolabel', 'tool_custom_uploaduser'), get_string('generalinfo', 'tool_custom_uploaduser'));
        $function = ["new" => get_string('newuser', 'tool_custom_uploaduser'), "existing"=> get_string('existinguser','tool_custom_uploaduser') ] ;
        $functionselect = $mform->addElement('select', 'func', 'Type of upload', $function);
        $mform->addRule('func', null, 'required', null, 'client');


        $mform->addElement('header', 'settingsheader', get_string('course'));
        $mform->setExpanded('settingsheader', true);
        $courses = get_courses_by_role();
        $roles = get_user_role();
		$group = "";

        $mform->addElement('select', 'courseid', get_string('selectcourse', 'tool_custom_uploaduser'), $courses);
        // $mform->addRule('courseid', null, 'required', null, 'client');
        $mform->addRule('courseid', null, 'required', null, 'client');
        $mform->addHelpButton('courseid', 'coursehelp','tool_custom_uploaduser');
        $mform->addElement('select', 'roleid', get_string('selectrole', 'tool_custom_uploaduser'), $roles);
        
        if($this->_customdata['courseid']){
			$mform->setDefault('courseid', $this->_customdata['courseid']);
			$mform->setDefault('roleid', $this->_customdata['roleid']);
			$groupobj = $DB->get_records('groups',array('courseid'=> $this->_customdata['courseid']));
			$group= array('0'=>"Select");

			if ($groupobj) {

				foreach($groupobj as $groups){
				$group[$groups->id] = $groups->name; 
				}
			}
			
		}
        $mform->addElement('select', 'groupid', get_string('selectgroup', 'tool_custom_uploaduser'), $group);
       
        $mform->addElement('text', 'newgroup', get_string('creategroup', 'tool_custom_uploaduser'));
        $mform->disabledIf('newgroup', 'groupid', 'neq', '1');

        $mform->setType('newgroup', PARAM_RAW);
        
        if($this->_customdata['courseid']){
			$mform->setDefault('groupid', $this->_customdata['groupid']);
		}

        $mform->addElement('header', 'optional', get_string('importuserinfo','tool_custom_uploaduser'));
        $mform->addElement('static', 'description', get_string('staticuserinfolabel', 'tool_custom_uploaduser'), get_string('staticuserinfo', 'tool_custom_uploaduser'));

        $mform->addElement('filepicker', 'userfile', get_string('file'));

		$mform->addElement('textarea', 'fromexcel', get_string('fromexcel','tool_custom_uploaduser'), 'rows="10" cols="50"');
		$mform->setType('fromexcel', PARAM_RAW);
		$mform->addHelpButton('fromexcel', 'fromexcel','tool_custom_uploaduser');

		$buttonarray=array();
		$buttonarray[] = &$mform->createElement('submit', 'submitbutton', get_string('importdata','tool_custom_uploaduser') );
		$buttonarray[] = &$mform->createElement('submit', 'addbutton',get_string('oradd','tool_custom_uploaduser'));
		$mform->addGroup($buttonarray, 'buttonar', '', array(' '), false);
		$mform->closeHeaderBefore('buttonar');
		
       //$this->add_action_buttons(false, get_string('importdata', 'tool_custom_uploaduser'));
    }
}
