
(function ($) {
    function fillgroup() {
        
        var selectedcourseid = $("#id_courseid").val();  
        console.log(selectedcourseid);  
        var groupid = $("#id_groupid").val();  
        console.log(groupid);  

            $.getJSON("./load_group.php?value="+selectedcourseid, function( data ) {
                
                var items = [];
                $.each( data, function( key, val ) {
                    if(groupid == key)
                        items.push( "<option value='" + key + "' selected>" + val + "</option>" );
                    else
                        items.push( "<option value='" + key + "'>" + val + "</option>" );
                });
                $(".fitem select[name='groupid']").html( items.join( "" ) );
            });     
    }

    function isValidEmailAddress(emailAddress) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if(regex.test(emailAddress)){
            return true;
        }else{
            return false;
        }
    }


    function fillprocessinfo(){
        if($("#processinfo").length){
            var func = $("[name='func']").attr('value');
            var groupid = $("[name='groupid']").attr('value');
            var courseid = $("[name='courseid']").attr('value');
            var role = $("[name='roleid']").attr('value');

            if(role == 5){
                var rolename = "students";
            }else{
                var rolename = "champions";
            }
            processhtml = "";

            if(func == "existing"){
                processhtml += 'Enrolling <b>existing users</b>';
            }else if(func == "new"){
                processhtml += 'Creating <b>new users</b>';
            }

            if(rolename == "students"){
                processhtml += ' as <b>students</b>';
            }else if(rolename == "champions"){
                processhtml += ' as <b>champions</b>';
            }


            $.getJSON("./load_process.php?groupid="+groupid+"&courseid="+courseid, function( data ) {
                // console.log(data);
                if(data['coursename'].length && data['groupname'].length){
                    processhtml += ' onto course: <b>'+data['coursename']+'</b> and into group: <b>'+data['groupname']+'</b>';
                    $("#processinfo").html(processhtml);
                }else if(data['coursename'].length){
                    processhtml += ' onto course: <b>'+data['coursename']+'.</b>';
                    $("#processinfo").html(processhtml);
                }else{
                    $("#processinfo").html(processhtml);
                    console.log("fail");
                }
            });

            
        }
        
    }





    function filluser(email, thisuserid, rawemail) {
        
        //What are we doing, enrolling new or existing users
        func = $("[name='func']").attr('value');


        $.getJSON("./load_user.php?email="+email, function( data ) {
            
            if(data['firstname']){
                //User exists. Fill.
                $("#id_firstname"+thisuserid).val(data['firstname']);
                $("#id_lastname"+thisuserid).val(data['lastname']);
                $("#id_password"+thisuserid).val("N/A");
                $("#id_email"+thisuserid).closest(".row").find(".notif").css("display", "none");
                if(func == "existing"){
                    $("#id_email"+thisuserid).closest(".row").find(".notif").css("display", "none");
                    $("#id_password"+thisuserid).parents().eq(1).after("<span class='notif statusgreen'>Valid</span>");
                }else{
                    $("#id_email"+thisuserid).closest(".row").find(".notif").css("display", "none");
                    $("#id_password"+thisuserid).parents().eq(1).after("<span class='notif statusred'>User exists</span>");
                }
            }else{
                //User does not exist
                if(func == "new"){
                    if(isValidEmailAddress(rawemail)){
                        $("#id_email"+thisuserid).closest(".row").find(".notif").css("display", "none");
                        $("#id_password"+thisuserid).parents().eq(1).after("<span class='notif statusgreen'>Valid</span>");
                    }else{
                        $("#id_email"+thisuserid).closest(".row").find(".notif").css("display", "none");
                        $("#id_password"+thisuserid).parents().eq(1).after("<span class='notif statusred'>Invalid email</span>");
                    }
                }else{
                    $("#id_email"+thisuserid).closest(".row").find(".notif").css("display", "none");
                   $("#id_password"+thisuserid).parents().eq(1).after("<span class='notif statusred'>User not found</span>");
                }
            }


        });     
    }

    function addrows(){
        $("[id*='id_email']").each(function(){
            thisemail = $(this).val();
            thishtmlid = $(this).attr("id");
            thisuserid = thishtmlid.replace("id_email", "");
            //console.log(thisemail);
            if(thisemail.length > 2){
                filluser(encodeURIComponent(thisemail), thisuserid, thisemail);  
            }
            
        });
    }


    $( document ).ready(function() {
        fillgroup();
        $("#id_courseid").change(function(){
                fillgroup();
        });
        
        $("#submitForm").click(function() {
             $("#deleteform").submit();

        });

        $(".open-homeEvents .btn.btn-secondary").click(function() {
             var rowId = $(this).data('id');
             $(".modal-body input[name='id']").attr('value', rowId);
            });

        addrows();
        fillprocessinfo();
        $("[id*='id_email']").blur(function () {
            thisemail = $(this).val();
            thishtmlid = $(this).attr("id");
            thisuserid = thishtmlid.replace("id_email", "");
            //console.log(thisemail);
            filluser(encodeURIComponent(thisemail), thisuserid, thisemail);

        });

        // $("#id_option_add_fields").click(function(){})
        
    });
}(jQuery));

