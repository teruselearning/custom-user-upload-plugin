<?php

defined('MOODLE_INTERNAL') || die();

$capabilities = array(

    // Allows the user to upload user pictures.
    'tool/custom_uploaduser:uploadusers' => array(
        'riskbitmask' => RISK_SPAM,
        'captype' => 'write',
        'contextlevel' => CONTEXT_SYSTEM,
        'archetypes' => array(
            'manager' => CAP_ALLOW
        ),
        'clonepermissionsfrom' =>  'moodle/site:uploadusers',
    ),
);
